// FormatterText.h
using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Data;
using namespace System::Runtime::Serialization;
using namespace System::Runtime::Serialization::Formatters::Binary;
using namespace System::Runtime::Serialization::Formatters;
using namespace System::Drawing;
using namespace System::IO;
#pragma once

using namespace System;


namespace FormatterText {
	[Serializable]
	public ref class StructChat
	{
	public:
		int type;
		String^ textChat;
		System::Drawing::Color^ color;
		System::Drawing::Font^ font;
	public: 
		StructChat()
		{
			type = 0;
			textChat = "";
			color = Color::BlueViolet;
			font = gcnew System::Drawing::Font("Arial", 9, FontStyle::Regular);
		}

		StructChat(int t, String^ text, System::Drawing::Color^ c, System::Drawing::Font^ f)
		{
			type = t;
			textChat = text;
			color = c;
			font = f;
		}
		StructChat(SerializationInfo^ info, StreamingContext^ strctx)
		{
			type = (int)info->GetValue("type", int::typeid);
			textChat = (String^)info->GetValue("text", String::typeid);
			font = (System::Drawing::Font^)info->GetValue("font", System::Drawing::Font::typeid);
			color = (System::Drawing::Color^)info->GetValue("color", System::Drawing::Color::typeid);
		}

		void getObjData(SerializationInfo^ info, StreamingContext^ strctx)
		{
			info->AddValue("type", type);
			info->AddValue("text", textChat);
			info->AddValue("font", font);
			info->AddValue("color", color);
		}
		StructChat^ Deserialize(array<unsigned char>^ buff)
		{
				MemoryStream^ ms = gcnew MemoryStream(buff);
				BinaryFormatter^ bformat = gcnew BinaryFormatter();
				return (StructChat^)bformat->Deserialize(ms);
		}
		array<unsigned char, 1>^ Serialize()
		{
				MemoryStream^ ms = gcnew MemoryStream();
				BinaryFormatter^ bformat = gcnew BinaryFormatter();
				bformat->Serialize(ms, this);
				return ms->ToArray();
		}
	};
}
