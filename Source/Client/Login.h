#pragma once

#include "Chat.h"
#include "Register.h"

namespace Client {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace FormatterText;
	/// <summary>
	/// Summary for Login
	/// </summary>
	public ref class Login : public System::Windows::Forms::Form
	{
	public:
		Login(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Login()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^  label1;
	protected:
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::TextBox^  tb_user;
	private: System::Windows::Forms::TextBox^  tb_pass;
	private: System::Windows::Forms::LinkLabel^  ll_register;
	private: System::Windows::Forms::Button^  bt_signin;
	private: System::ComponentModel::BackgroundWorker^  backgroundWorker1;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->tb_user = (gcnew System::Windows::Forms::TextBox());
			this->tb_pass = (gcnew System::Windows::Forms::TextBox());
			this->ll_register = (gcnew System::Windows::Forms::LinkLabel());
			this->bt_signin = (gcnew System::Windows::Forms::Button());
			this->backgroundWorker1 = (gcnew System::ComponentModel::BackgroundWorker());
			this->SuspendLayout();
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10));
			this->label1->Location = System::Drawing::Point(24, 63);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(73, 17);
			this->label1->TabIndex = 0;
			this->label1->Text = L"Username";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10));
			this->label2->Location = System::Drawing::Point(24, 93);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(69, 17);
			this->label2->TabIndex = 1;
			this->label2->Text = L"Password";
			// 
			// tb_user
			// 
			this->tb_user->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10));
			this->tb_user->Location = System::Drawing::Point(126, 63);
			this->tb_user->Name = L"tb_user";
			this->tb_user->Size = System::Drawing::Size(199, 23);
			this->tb_user->TabIndex = 2;
			// 
			// tb_pass
			// 
			this->tb_pass->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10));
			this->tb_pass->Location = System::Drawing::Point(126, 93);
			this->tb_pass->Name = L"tb_pass";
			this->tb_pass->PasswordChar = '*';
			this->tb_pass->Size = System::Drawing::Size(199, 23);
			this->tb_pass->TabIndex = 3;
			// 
			// ll_register
			// 
			this->ll_register->AutoSize = true;
			this->ll_register->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10));
			this->ll_register->Location = System::Drawing::Point(264, 27);
			this->ll_register->Name = L"ll_register";
			this->ll_register->Size = System::Drawing::Size(61, 17);
			this->ll_register->TabIndex = 4;
			this->ll_register->TabStop = true;
			this->ll_register->Text = L"Register";
			this->ll_register->LinkClicked += gcnew System::Windows::Forms::LinkLabelLinkClickedEventHandler(this, &Login::ll_register_LinkClicked);
			// 
			// bt_signin
			// 
			this->bt_signin->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10));
			this->bt_signin->Location = System::Drawing::Point(137, 142);
			this->bt_signin->Name = L"bt_signin";
			this->bt_signin->Size = System::Drawing::Size(75, 30);
			this->bt_signin->TabIndex = 5;
			this->bt_signin->Text = L"Sign in";
			this->bt_signin->UseVisualStyleBackColor = true;
			this->bt_signin->Click += gcnew System::EventHandler(this, &Login::bt_signin_Click);
			// 
			// backgroundWorker1
			// 
			this->backgroundWorker1->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &Login::backgroundWorker1_DoWork);
			// 
			// Login
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(350, 211);
			this->Controls->Add(this->bt_signin);
			this->Controls->Add(this->ll_register);
			this->Controls->Add(this->tb_pass);
			this->Controls->Add(this->tb_user);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Name = L"Login";
			this->ShowInTaskbar = false;
			this->Text = L"Login";
			this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &Login::Login_FormClosing);
			this->FormClosed += gcnew System::Windows::Forms::FormClosedEventHandler(this, &Login::Login_FormClosed);
			this->Load += gcnew System::EventHandler(this, &Login::Login_Load);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	public: static int loop = 0;
	public: SocketClient^ skCl = gcnew SocketClient;
	public: Socket^ client = skCl->sk;
	private: System::Void bt_signin_Click(System::Object^  sender, System::EventArgs^  e) {
				 String^ user = tb_user->Text;
				 String^ pass = tb_pass->Text;

				 if (user->Length < 5 || pass->Length < 5)
					 MessageBox::Show("Your user or pass is wrong!", "Error", MessageBoxButtons::OK);
				 else
				 {
					 array<unsigned char>^ buff = gcnew array<unsigned char>(1024);
					 FormatterText::StructChat^ str = gcnew FormatterText::StructChat();
					 str->type = 1;
					 str->textChat = user + " " + pass;
					 buff = str->Serialize();
					 try
					 {
						 client->Send(buff, buff->Length, SocketFlags::None);
						 skCl->user = user;
						 backgroundWorker1->RunWorkerAsync();	
						 this->Close();
					 }
					 catch (Exception^ ex)
					 {
						 MessageBox::Show(ex->Message);
					 }
				 }
	}
private: System::Void ll_register_LinkClicked(System::Object^  sender, System::Windows::Forms::LinkLabelLinkClickedEventArgs^  e) {
			 Register^ rs = gcnew Register();
			 rs->Activate();
			 this->Close();
			 rs->Show();
}
private: System::Void backgroundWorker1_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e) {
			 while (true)
			 {
				 array <unsigned char>^ buff = gcnew array<unsigned char>(1024);
				 int recv = client->Receive(buff);
				 FormatterText::StructChat^ str = gcnew FormatterText::StructChat();
				 str = str->Deserialize(buff);
				 if (str->type == 0)
				 {
					 Chat^ ch = gcnew Chat();
					 ch->Activate();
					 ch->ShowDialog();
					 loop = 1;
					 break;
				 }
				 else
				 {
					 loop = 0;
					 MessageBox::Show("Your user or pass is wrong!", "Error", MessageBoxButtons::OK);
					 if (backgroundWorker1->IsBusy)
					 {
						 backgroundWorker1->CancelAsync();
					 }
				 }	 
			 }
			 if (backgroundWorker1->IsBusy)
			 {
				 backgroundWorker1->CancelAsync();
			 }
}
private: System::Void Login_Load(System::Object^  sender, System::EventArgs^  e) {
}
private: System::Void Login_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e) {
	//		 if (backgroundWorker1->IsBusy)
	//		 {
	//			 backgroundWorker1->CancelAsync();
	//		 }
}
private: System::Void Login_FormClosed(System::Object^  sender, System::Windows::Forms::FormClosedEventArgs^  e) {
			 //if (backgroundWorker1->IsBusy)
			//	 backgroundWorker1->CancelAsync();
}
};
}
