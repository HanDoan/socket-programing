#ifndef REGISTER_H
#define REGISTER_H
//#include "Login.h"
#include "Chat.h"

#pragma once
namespace Client {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for Register
	/// </summary>
	public ref class Register : public System::Windows::Forms::Form
	{
	public:
		Register(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Register()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^  label1;
	protected:
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::TextBox^  tb_user;
	private: System::Windows::Forms::TextBox^  tb_pass;
	private: System::Windows::Forms::TextBox^  tb_cfpass;
	private: System::Windows::Forms::Button^  bt_next;
	private: System::ComponentModel::BackgroundWorker^  backgroundWorker1;


	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->tb_user = (gcnew System::Windows::Forms::TextBox());
			this->tb_pass = (gcnew System::Windows::Forms::TextBox());
			this->tb_cfpass = (gcnew System::Windows::Forms::TextBox());
			this->bt_next = (gcnew System::Windows::Forms::Button());
			this->backgroundWorker1 = (gcnew System::ComponentModel::BackgroundWorker());
			this->SuspendLayout();
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10));
			this->label1->Location = System::Drawing::Point(13, 40);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(73, 17);
			this->label1->TabIndex = 0;
			this->label1->Text = L"Username";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10));
			this->label2->Location = System::Drawing::Point(13, 71);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(69, 17);
			this->label2->TabIndex = 1;
			this->label2->Text = L"Password";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10));
			this->label3->Location = System::Drawing::Point(13, 102);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(120, 17);
			this->label3->TabIndex = 2;
			this->label3->Text = L"Confirm password";
			// 
			// tb_user
			// 
			this->tb_user->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10));
			this->tb_user->Location = System::Drawing::Point(144, 40);
			this->tb_user->Name = L"tb_user";
			this->tb_user->Size = System::Drawing::Size(189, 23);
			this->tb_user->TabIndex = 3;
			// 
			// tb_pass
			// 
			this->tb_pass->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10));
			this->tb_pass->Location = System::Drawing::Point(144, 71);
			this->tb_pass->Name = L"tb_pass";
			this->tb_pass->PasswordChar = '*';
			this->tb_pass->Size = System::Drawing::Size(189, 23);
			this->tb_pass->TabIndex = 4;
			// 
			// tb_cfpass
			// 
			this->tb_cfpass->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10));
			this->tb_cfpass->Location = System::Drawing::Point(144, 102);
			this->tb_cfpass->Name = L"tb_cfpass";
			this->tb_cfpass->PasswordChar = '*';
			this->tb_cfpass->Size = System::Drawing::Size(189, 23);
			this->tb_cfpass->TabIndex = 5;
			// 
			// bt_next
			// 
			this->bt_next->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10));
			this->bt_next->Location = System::Drawing::Point(133, 155);
			this->bt_next->Name = L"bt_next";
			this->bt_next->Size = System::Drawing::Size(75, 28);
			this->bt_next->TabIndex = 6;
			this->bt_next->Text = L"Next";
			this->bt_next->UseVisualStyleBackColor = true;
			this->bt_next->Click += gcnew System::EventHandler(this, &Register::bt_signin_Click);
			// 
			// backgroundWorker1
			// 
			this->backgroundWorker1->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &Register::backgroundWorker1_DoWork);
			// 
			// Register
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(357, 213);
			this->Controls->Add(this->bt_next);
			this->Controls->Add(this->tb_cfpass);
			this->Controls->Add(this->tb_pass);
			this->Controls->Add(this->tb_user);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Name = L"Register";
			this->ShowInTaskbar = false;
			this->Text = L"Register";
			this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &Register::Register_FormClosing);
			this->FormClosed += gcnew System::Windows::Forms::FormClosedEventHandler(this, &Register::Register_FormClosed);
			this->Load += gcnew System::EventHandler(this, &Register::Register_Load);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
	
#pragma endregion
		public: SocketClient^ skCl = gcnew SocketClient;
		public:	Socket^ client = skCl->sk;
		private: System::Void bt_signin_Click(System::Object^  sender, System::EventArgs^  e) {	
					 String^ user = tb_user->Text;
					 String^ pass = tb_pass->Text;
					 String^ cfpass = tb_cfpass->Text;
					
					 if (user->Length < 5 || pass->Length < 5)
						 MessageBox::Show("Username or password must be more than 4 characters!", "Error", MessageBoxButtons::OK);
					 else if (pass != cfpass)
						 MessageBox::Show("These pass don't match!", "Error", MessageBoxButtons::OK);
					 else if (user->Contains(" "))
						 MessageBox::Show("Username don't have special characters!", "Error", MessageBoxButtons::OK);
					 else
					 {
						 array<unsigned char>^ buff = gcnew array<unsigned char>(1024);
						 FormatterText::StructChat^ str = gcnew FormatterText::StructChat();
						 str->type = 2;
						 str->textChat = user + " " + pass;
						 buff = str->Serialize();
						 try
						 {
							 skCl->user = user;
							 client->Send(buff, buff->Length, SocketFlags::None);
							 backgroundWorker1->RunWorkerAsync();
						 }
						 catch (Exception^ ex)
						 {
							 MessageBox::Show(ex->Message);
						 }
					 }			
		}
	private: System::Void backgroundWorker1_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e) {
				 while (true)
				 {
					 array <unsigned char>^ buff = gcnew array<unsigned char>(1024);
					 int recv = client->Receive(buff);
					 FormatterText::StructChat^ str = gcnew FormatterText::StructChat();
					 str = str->Deserialize(buff);
					 if (str->type == 0)
					 {
						 Chat^ ch = gcnew Chat();
						 ch->Activate();
						 ch->ShowDialog();
						 break;
					 }
					 else
					 {
						 MessageBox::Show("Username already exists!", "Error", MessageBoxButtons::OK);
						 if (backgroundWorker1->IsBusy)
						 {
							 backgroundWorker1->CancelAsync();
						 }
						 
					 }
				 }
				 if (backgroundWorker1->IsBusy)
				 {
					 backgroundWorker1->CancelAsync();
				 }
	}
private: System::Void Register_Load(System::Object^  sender, System::EventArgs^  e) {
}
private: System::Void Register_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e) {
}
private: System::Void Register_FormClosed(System::Object^  sender, System::Windows::Forms::FormClosedEventArgs^  e) {		 
}
};
}
#endif //REGISTER_H