#pragma once
#include "Login.h"

namespace Client {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Net;
	using namespace System::Net::Sockets;
	using namespace System::Runtime::Serialization;
	using namespace System::Runtime::Serialization::Formatters::Binary;
	using namespace System::Runtime::Serialization::Formatters;
	using namespace System::Threading;
	using namespace System::IO;
	/// <summary>
	/// Summary for Connect
	/// </summary>
	public ref class Connect : public System::Windows::Forms::Form
	{
	public:
		Connect(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Connect()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^  bt_connect;
	protected:
	private: System::Windows::Forms::TextBox^  tb_ip;
	private: System::Windows::Forms::Label^  label1;

	private: System::Windows::Forms::Button^  bt_disconnect;

	protected:



	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->bt_connect = (gcnew System::Windows::Forms::Button());
			this->tb_ip = (gcnew System::Windows::Forms::TextBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->bt_disconnect = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// bt_connect
			// 
			this->bt_connect->Location = System::Drawing::Point(368, 20);
			this->bt_connect->Name = L"bt_connect";
			this->bt_connect->Size = System::Drawing::Size(75, 23);
			this->bt_connect->TabIndex = 6;
			this->bt_connect->Text = L"Connect";
			this->bt_connect->UseVisualStyleBackColor = true;
			this->bt_connect->Click += gcnew System::EventHandler(this, &Connect::bt_connect_Click);
			// 
			// tb_ip
			// 
			this->tb_ip->Location = System::Drawing::Point(78, 23);
			this->tb_ip->Name = L"tb_ip";
			this->tb_ip->Size = System::Drawing::Size(284, 20);
			this->tb_ip->TabIndex = 5;
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(14, 26);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(58, 13);
			this->label1->TabIndex = 4;
			this->label1->Text = L"IP Address";
			// 
			// bt_disconnect
			// 
			this->bt_disconnect->Location = System::Drawing::Point(368, 20);
			this->bt_disconnect->Name = L"bt_disconnect";
			this->bt_disconnect->Size = System::Drawing::Size(75, 23);
			this->bt_disconnect->TabIndex = 7;
			this->bt_disconnect->Text = L"Disconnect";
			this->bt_disconnect->UseVisualStyleBackColor = true;
			this->bt_disconnect->Click += gcnew System::EventHandler(this, &Connect::bt_disconnect_Click);
			// 
			// Connect
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(452, 66);
			this->Controls->Add(this->bt_disconnect);
			this->Controls->Add(this->bt_connect);
			this->Controls->Add(this->tb_ip);
			this->Controls->Add(this->label1);
			this->Name = L"Connect";
			this->Text = L"Connect";
			this->Load += gcnew System::EventHandler(this, &Connect::Connect_Load);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	public: IPEndPoint ^ ipe;
	public: SocketClient^ skCl;
	private: System::Void bt_connect_Click(System::Object^  sender, System::EventArgs^  e) {
				 bt_connect->Hide();
				 bt_disconnect->Show();
				 skCl = gcnew SocketClient;
				 skCl->sk = gcnew Socket(AddressFamily::InterNetwork, SocketType::Stream, ProtocolType::Tcp);
				 try
				 {
					 ipe = gcnew IPEndPoint(IPAddress::Parse(tb_ip->Text), 0511);
					 skCl->sk->Connect(ipe); 
					 Login^ lg = gcnew Login;
					 lg->Activate();
					 lg->Show();
					 return;
				 }
				 catch (Exception^ ex)
				 {
					 MessageBox::Show(ex->Message);
				 }
	}
	private: System::Void Connect_Load(System::Object^  sender, System::EventArgs^  e) {
				 bt_disconnect->Hide();			 
	}
private: System::Void bt_disconnect_Click(System::Object^  sender, System::EventArgs^  e) {
			 array<unsigned char>^ buff = gcnew array<unsigned char>(1024);
			 FormatterText::StructChat^ str = gcnew FormatterText::StructChat();
			 str->type = DISCONNECT;
			 buff = str->Serialize();
			 Socket^ sk = skCl->sk;
			 sk->Send(buff, buff->Length, SocketFlags::None);
			 bt_connect->Show();
			 bt_disconnect->Hide();
			 delete skCl;
}
};
}
