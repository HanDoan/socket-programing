#pragma once
#include <Windows.h>
#include <msclr/marshal.h>
#include "Header.h"
#include <stdio.h>

namespace Client {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Net;
	using namespace System::Net::Sockets;
	using namespace System::Runtime::Serialization;
	using namespace System::Runtime::Serialization::Formatters::Binary;
	using namespace System::Runtime::Serialization::Formatters;
	using namespace System::Threading;
	using namespace System::IO;
	using namespace FormatterText;
	using namespace System::Runtime::InteropServices;
	/// <summary>
	/// Summary for Chat
	/// </summary>
	public ref class Chat : public System::Windows::Forms::Form
	{
	public:
		Chat(void)
		{

			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
			CheckForIllegalCrossThreadCalls == false;
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Chat()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::MenuStrip^  menuStrip1;
	protected:
	private: System::Windows::Forms::ToolStripMenuItem^  chattingToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  aboutToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  helpToolStripMenuItem;



	private: System::Windows::Forms::RichTextBox^  rtb_main;

	private: System::Windows::Forms::RichTextBox^  rtb_textChat;
	private: System::Windows::Forms::Button^  bt_send;
	private: System::Windows::Forms::LinkLabel^  ll_font;
	private: System::Windows::Forms::LinkLabel^  ll_color;
	private: System::Windows::Forms::LinkLabel^  ll_file;
	private: System::Windows::Forms::ColorDialog^  colorDialog1;
	private: System::Windows::Forms::FontDialog^  fontDialog1;
	private: System::Windows::Forms::ToolStripMenuItem^  privateChatToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  exitToolStripMenuItem;
	private: System::ComponentModel::BackgroundWorker^  backgroundWorker1;

	private: System::Windows::Forms::RichTextBox^  rtb_pm;

	private: System::Windows::Forms::TextBox^  tb_user;
	private: System::Windows::Forms::Button^  bt_sendto;

	private: System::Windows::Forms::ToolStripMenuItem^  publicChatToolStripMenuItem;
	private: System::Windows::Forms::TextBox^  tb_chatting;
	private: System::Windows::Forms::RichTextBox^  rtb_userpm;
	private: System::Windows::Forms::OpenFileDialog^  openFileDialog1;



	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->chattingToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->privateChatToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->publicChatToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->exitToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->aboutToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->helpToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->rtb_main = (gcnew System::Windows::Forms::RichTextBox());
			this->rtb_textChat = (gcnew System::Windows::Forms::RichTextBox());
			this->bt_send = (gcnew System::Windows::Forms::Button());
			this->ll_font = (gcnew System::Windows::Forms::LinkLabel());
			this->ll_color = (gcnew System::Windows::Forms::LinkLabel());
			this->ll_file = (gcnew System::Windows::Forms::LinkLabel());
			this->colorDialog1 = (gcnew System::Windows::Forms::ColorDialog());
			this->fontDialog1 = (gcnew System::Windows::Forms::FontDialog());
			this->backgroundWorker1 = (gcnew System::ComponentModel::BackgroundWorker());
			this->rtb_pm = (gcnew System::Windows::Forms::RichTextBox());
			this->tb_user = (gcnew System::Windows::Forms::TextBox());
			this->bt_sendto = (gcnew System::Windows::Forms::Button());
			this->tb_chatting = (gcnew System::Windows::Forms::TextBox());
			this->rtb_userpm = (gcnew System::Windows::Forms::RichTextBox());
			this->openFileDialog1 = (gcnew System::Windows::Forms::OpenFileDialog());
			this->menuStrip1->SuspendLayout();
			this->SuspendLayout();
			// 
			// menuStrip1
			// 
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {
				this->chattingToolStripMenuItem,
					this->aboutToolStripMenuItem
			});
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(565, 24);
			this->menuStrip1->TabIndex = 0;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// chattingToolStripMenuItem
			// 
			this->chattingToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {
				this->privateChatToolStripMenuItem,
					this->publicChatToolStripMenuItem, this->exitToolStripMenuItem
			});
			this->chattingToolStripMenuItem->Name = L"chattingToolStripMenuItem";
			this->chattingToolStripMenuItem->Size = System::Drawing::Size(65, 20);
			this->chattingToolStripMenuItem->Text = L"&Chatting";
			// 
			// privateChatToolStripMenuItem
			// 
			this->privateChatToolStripMenuItem->Name = L"privateChatToolStripMenuItem";
			this->privateChatToolStripMenuItem->Size = System::Drawing::Size(136, 22);
			this->privateChatToolStripMenuItem->Text = L"&Private chat";
			this->privateChatToolStripMenuItem->Click += gcnew System::EventHandler(this, &Chat::privateChatToolStripMenuItem_Click);
			// 
			// publicChatToolStripMenuItem
			// 
			this->publicChatToolStripMenuItem->Name = L"publicChatToolStripMenuItem";
			this->publicChatToolStripMenuItem->Size = System::Drawing::Size(136, 22);
			this->publicChatToolStripMenuItem->Text = L"Public &chat";
			this->publicChatToolStripMenuItem->Click += gcnew System::EventHandler(this, &Chat::publicChatToolStripMenuItem_Click);
			// 
			// exitToolStripMenuItem
			// 
			this->exitToolStripMenuItem->Name = L"exitToolStripMenuItem";
			this->exitToolStripMenuItem->Size = System::Drawing::Size(136, 22);
			this->exitToolStripMenuItem->Text = L"&Exit";
			this->exitToolStripMenuItem->Click += gcnew System::EventHandler(this, &Chat::exitToolStripMenuItem_Click);
			// 
			// aboutToolStripMenuItem
			// 
			this->aboutToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->helpToolStripMenuItem });
			this->aboutToolStripMenuItem->Name = L"aboutToolStripMenuItem";
			this->aboutToolStripMenuItem->Size = System::Drawing::Size(52, 20);
			this->aboutToolStripMenuItem->Text = L"A&bout";
			// 
			// helpToolStripMenuItem
			// 
			this->helpToolStripMenuItem->Name = L"helpToolStripMenuItem";
			this->helpToolStripMenuItem->Size = System::Drawing::Size(99, 22);
			this->helpToolStripMenuItem->Text = L"Help";
			// 
			// rtb_main
			// 
			this->rtb_main->Location = System::Drawing::Point(18, 58);
			this->rtb_main->Name = L"rtb_main";
			this->rtb_main->Size = System::Drawing::Size(345, 169);
			this->rtb_main->TabIndex = 4;
			this->rtb_main->Text = L"";
			this->rtb_main->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Chat::rtb_main_KeyPress);
			// 
			// rtb_textChat
			// 
			this->rtb_textChat->Location = System::Drawing::Point(18, 251);
			this->rtb_textChat->Name = L"rtb_textChat";
			this->rtb_textChat->Size = System::Drawing::Size(345, 51);
			this->rtb_textChat->TabIndex = 0;
			this->rtb_textChat->Text = L"";
			// 
			// bt_send
			// 
			this->bt_send->Location = System::Drawing::Point(369, 251);
			this->bt_send->Name = L"bt_send";
			this->bt_send->Size = System::Drawing::Size(75, 51);
			this->bt_send->TabIndex = 6;
			this->bt_send->Text = L"Send";
			this->bt_send->UseVisualStyleBackColor = true;
			this->bt_send->Click += gcnew System::EventHandler(this, &Chat::bt_send_Click);
			// 
			// ll_font
			// 
			this->ll_font->AutoSize = true;
			this->ll_font->Location = System::Drawing::Point(15, 235);
			this->ll_font->Name = L"ll_font";
			this->ll_font->Size = System::Drawing::Size(28, 13);
			this->ll_font->TabIndex = 7;
			this->ll_font->TabStop = true;
			this->ll_font->Text = L"Font";
			this->ll_font->LinkClicked += gcnew System::Windows::Forms::LinkLabelLinkClickedEventHandler(this, &Chat::ll_font_LinkClicked);
			// 
			// ll_color
			// 
			this->ll_color->AutoSize = true;
			this->ll_color->Location = System::Drawing::Point(58, 235);
			this->ll_color->Name = L"ll_color";
			this->ll_color->Size = System::Drawing::Size(31, 13);
			this->ll_color->TabIndex = 8;
			this->ll_color->TabStop = true;
			this->ll_color->Text = L"Color";
			this->ll_color->LinkClicked += gcnew System::Windows::Forms::LinkLabelLinkClickedEventHandler(this, &Chat::ll_color_LinkClicked);
			// 
			// ll_file
			// 
			this->ll_file->AutoSize = true;
			this->ll_file->Location = System::Drawing::Point(111, 235);
			this->ll_file->Name = L"ll_file";
			this->ll_file->Size = System::Drawing::Size(23, 13);
			this->ll_file->TabIndex = 9;
			this->ll_file->TabStop = true;
			this->ll_file->Text = L"File";
			this->ll_file->LinkClicked += gcnew System::Windows::Forms::LinkLabelLinkClickedEventHandler(this, &Chat::ll_file_LinkClicked);
			// 
			// backgroundWorker1
			// 
			this->backgroundWorker1->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &Chat::backgroundWorker1_DoWork);
			// 
			// rtb_pm
			// 
			this->rtb_pm->Location = System::Drawing::Point(369, 58);
			this->rtb_pm->Name = L"rtb_pm";
			this->rtb_pm->Size = System::Drawing::Size(181, 169);
			this->rtb_pm->TabIndex = 10;
			this->rtb_pm->Text = L"";
			this->rtb_pm->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Chat::rtb_pm_KeyPress);
			// 
			// tb_user
			// 
			this->tb_user->BackColor = System::Drawing::SystemColors::Control;
			this->tb_user->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->tb_user->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10));
			this->tb_user->ForeColor = System::Drawing::Color::DarkOrchid;
			this->tb_user->Location = System::Drawing::Point(18, 31);
			this->tb_user->Name = L"tb_user";
			this->tb_user->Size = System::Drawing::Size(116, 16);
			this->tb_user->TabIndex = 11;
			// 
			// bt_sendto
			// 
			this->bt_sendto->Location = System::Drawing::Point(369, 251);
			this->bt_sendto->Name = L"bt_sendto";
			this->bt_sendto->Size = System::Drawing::Size(75, 51);
			this->bt_sendto->TabIndex = 12;
			this->bt_sendto->Text = L"Send to";
			this->bt_sendto->UseVisualStyleBackColor = true;
			this->bt_sendto->Click += gcnew System::EventHandler(this, &Chat::bt_sendto_Click);
			// 
			// tb_chatting
			// 
			this->tb_chatting->BackColor = System::Drawing::SystemColors::Control;
			this->tb_chatting->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->tb_chatting->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Italic, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->tb_chatting->ForeColor = System::Drawing::SystemColors::Highlight;
			this->tb_chatting->Location = System::Drawing::Point(319, 31);
			this->tb_chatting->Name = L"tb_chatting";
			this->tb_chatting->ReadOnly = true;
			this->tb_chatting->Size = System::Drawing::Size(231, 15);
			this->tb_chatting->TabIndex = 14;
			this->tb_chatting->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			// 
			// rtb_userpm
			// 
			this->rtb_userpm->Location = System::Drawing::Point(450, 267);
			this->rtb_userpm->Name = L"rtb_userpm";
			this->rtb_userpm->Size = System::Drawing::Size(100, 27);
			this->rtb_userpm->TabIndex = 15;
			this->rtb_userpm->Text = L"";
			// 
			// openFileDialog1
			// 
			this->openFileDialog1->FileName = L"openFileDialog1";
			// 
			// Chat
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(565, 310);
			this->Controls->Add(this->rtb_userpm);
			this->Controls->Add(this->tb_chatting);
			this->Controls->Add(this->bt_sendto);
			this->Controls->Add(this->tb_user);
			this->Controls->Add(this->rtb_pm);
			this->Controls->Add(this->ll_file);
			this->Controls->Add(this->ll_color);
			this->Controls->Add(this->ll_font);
			this->Controls->Add(this->bt_send);
			this->Controls->Add(this->rtb_textChat);
			this->Controls->Add(this->rtb_main);
			this->Controls->Add(this->menuStrip1);
			this->MainMenuStrip = this->menuStrip1;
			this->Name = L"Chat";
			this->ShowInTaskbar = false;
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"Chat";
			this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &Chat::Chat_FormClosing);
			this->FormClosed += gcnew System::Windows::Forms::FormClosedEventHandler(this, &Chat::Chat_FormClosed);
			this->Load += gcnew System::EventHandler(this, &Chat::Chat_Load);
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void ll_font_LinkClicked(System::Object^  sender, System::Windows::Forms::LinkLabelLinkClickedEventArgs^  e) {
				 if (fontDialog1->ShowDialog() == System::Windows::Forms::DialogResult::OK)
				 {
					 rtb_textChat->Font = fontDialog1->Font;
				 }
	}
	private: System::Void ll_color_LinkClicked(System::Object^  sender, System::Windows::Forms::LinkLabelLinkClickedEventArgs^  e) {
				 if (colorDialog1->ShowDialog() == System::Windows::Forms::DialogResult::OK)
				 {
					 rtb_textChat->ForeColor = colorDialog1->Color;
				 }
	}
	private: System::Void exitToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
				 if (MessageBox::Show("Do you really want to exit application?", "Exit",
					 MessageBoxButtons::YesNo) == System::Windows::Forms::DialogResult::Yes)
				 {
					 if (backgroundWorker1->IsBusy)
						 backgroundWorker1->CancelAsync();
					 Application::Exit();
				 }

	}
	public: SocketClient^ skCl = gcnew SocketClient;
	public: Socket^ client = skCl->sk;
	public: Thread^ listenServer;
	public: String^ user = skCl->user;
	public: String^ userpm;
	private: System::Void Chat_Load(System::Object^  sender, System::EventArgs^  e) {
				 bt_sendto->Hide();
				 rtb_userpm->Hide();
				 tb_chatting->Text = "Chatting with everyone...";
				 ll_file->Hide();
				 tb_user->AppendText(user);
				 try
				 {
					 backgroundWorker1->RunWorkerAsync();
				 }
				 catch (Exception^ ex)
				 {
					 MessageBox::Show(ex->Message);
				 }
	}
			 
private: System::Void backgroundWorker1_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e) {
			 while (true)
			 {
				 array<unsigned char>^ buff = gcnew array<unsigned char>(1024);
				 int recv = client->Receive(buff);
				 FormatterText::StructChat^ str = gcnew FormatterText::StructChat();
				 str = str->Deserialize(buff);
				 if (str->type == CHAT)
					addText(str->textChat, str->font, str->color);
				 else if (str->type == PRIVATE_CHAT)
					 addPmText(str->textChat, str->font, str->color);
				 else if (str->type == SEND || str->type == MS_FILE || str->type == END_FILE)
				 {
					 FileStream^ file;
					 if (str->type == SEND)
					 {
						  file = gcnew FileStream(str->textChat, FileMode::Create, FileAccess::Write);
					 }
					 else if (str->type == MS_FILE)
					 {
						 array<wchar_t>^ ms = str->textChat->ToCharArray();
						 array<unsigned char>^ bf = gcnew array<unsigned char>(ms->Length);
						 for (int i = 0; ms->Length; ++i)
							 bf[i] = (unsigned char)ms[i];
						 file->Write(bf, file->Seek(0, SeekOrigin::End), MAXFILE);
					 }
					 else
					 {
						 file->Close();
						 delete file;
					 }				 
				 }				
				 else if (str->type == ERROR)
					 MessageBox::Show("Username is wrong. Try again! ", "Error", MessageBoxButtons::OK);
			 }
}
		 void addText(String^ text, System::Drawing::Font^ font, System::Drawing::Color^ color){
			 rtb_main->SelectionFont = font;
			 rtb_main->SelectionColor = (Color) color;
			 rtb_main->AppendText (text + "\n");
			 rtb_main->ScrollToCaret();
		 }
		 void addPmText(String^ text, System::Drawing::Font^ font, System::Drawing::Color^ color){
			 rtb_pm->SelectionFont = font;
			 rtb_pm->SelectionColor = (Color)color;
			 rtb_pm->AppendText(text + "\n");
			 rtb_pm->ScrollToCaret();
		 }
private: System::Void bt_send_Click(System::Object^  sender, System::EventArgs^  e) {
			 array<unsigned char>^ buff = gcnew array<unsigned char>(1024);
			 FormatterText::StructChat^ str = gcnew FormatterText::StructChat();
			 str->type = CHAT;
			 str->textChat = user + ": " + rtb_textChat->Text;
			 str->color = rtb_textChat->ForeColor;
			 str->font = rtb_textChat->Font;

			 buff = str->Serialize();
			 client->Send(buff, buff->Length, SocketFlags::None);
			 rtb_textChat->Text = "";

}
private: System::Void tb_ip_TextChanged(System::Object^  sender, System::EventArgs^  e) {
}
private: System::Void label1_Click(System::Object^  sender, System::EventArgs^  e) {
}
private: System::Void rtb_main_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {
			 e->Handled = true;
}
private: System::Void Chat_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e) {
			 if (backgroundWorker1->IsBusy)
				 backgroundWorker1->CancelAsync();
}
private: System::Void Chat_FormClosed(System::Object^  sender, System::Windows::Forms::FormClosedEventArgs^  e) {
			 if (backgroundWorker1->IsBusy)
				 backgroundWorker1->CancelAsync();
}
private: System::Void privateChatToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 bt_send->Hide();
			 rtb_userpm->Show();
			 bt_sendto->Show();
			 tb_chatting->Text = "Private Chatting...";
			 ll_file->Show();
}
private: System::Void publicChatToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 bt_send->Show();
			 rtb_userpm->Hide();
			 bt_sendto->Hide();
			 ll_file->Hide();
			 tb_chatting->Text ="Chatting with everyone...";
}
private: System::Void bt_sendto_Click(System::Object^  sender, System::EventArgs^  e) {
			 array<unsigned char>^ buff = gcnew array<unsigned char>(1024);
			 FormatterText::StructChat^ str = gcnew FormatterText::StructChat();
			 str->type = PRIVATE_CHAT;
			 str->textChat = rtb_userpm->Text + " " + user + ": " + rtb_textChat->Text;
			 str->color = rtb_textChat->ForeColor;
			 str->font = rtb_textChat->Font;
			 buff = str->Serialize();
			 client->Send(buff, buff->Length, SocketFlags::None);
			 rtb_textChat->Text = "";
}
private: System::Void rtb_pm_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {
			 e->Handled = true;
}
private: System::Void ll_file_LinkClicked(System::Object^  sender, System::Windows::Forms::LinkLabelLinkClickedEventArgs^  e) {
			 if (openFileDialog1->ShowDialog() == System::Windows::Forms::DialogResult::OK){}
			/* {
				 FileStream^ file = (FileStream^) openFileDialog1->OpenFile();
				 rtb_pm->AppendText(file->Name + '\n');
				 array<unsigned char>^ buff = gcnew array<unsigned char>(MAXFILE);
				 FormatterText::StructChat^ str = gcnew FormatterText::StructChat();
				 //Gui thong tin bat dau gui file
				 str->type = SEND;
				 str->textChat = rtb_userpm->Text + " " + file->Name;
				 buff = str->Serialize();
				 client->Send(buff, buff->Length, SocketFlags::None);

				 int nums = MAXFILE - 1 - rtb_userpm->Name->Length - 1;
				 int curSize = nums;
				 int offset = 0;
				 array<unsigned char>^ bfread = gcnew array<unsigned char>(MAXFILE);
				 //Gui file
				 do
				 {
					 curSize = file->Read(bfread, 0, nums);
					 bfread[curSize] = '\0';
					 str->type = MS_FILE;
					 str->textChat = rtb_userpm->Text + " " + bfread->ToString();
					 buff = str->Serialize();
					 client->Send(buff, buff->Length, SocketFlags::None);
				 } while (curSize >= nums);

				 //Gui thong tin ket thuc gui file
				 str->type = MS_FILE;
				 str->textChat = rtb_userpm->Text + " ";
				 buff = str->Serialize();
				 client->Send(buff, buff->Length, SocketFlags::None);
			 }*/
			 
}
};
}
