#pragma once

#define	SUCCESS	0
#define DISCONNECT -2
#define ERROR -1
#define SIGN_IN 1
#define SIGN_UP 2
#define CHAT	3
#define PRIVATE_CHAT 4
#define SEND 5
#define FILE 6
#define END_FILE 7
#define MAX 100
namespace Sever {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace FormatterText;
	using namespace System::Collections::Generic;
	using namespace System::Net;
	using namespace System::Net::Sockets;
	using namespace Threading;
	using namespace System::IO;
	using namespace System::Runtime::Serialization::Formatters::Binary;
	using namespace System::Runtime::Serialization::Formatters;
	/// <summary>
	/// Summary for Server
	/// </summary>
	public ref class Server : public System::Windows::Forms::Form
	{
	public:
		Server(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
			CheckForIllegalCrossThreadCalls = false;
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Server()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^  label1;
	protected:
	private: System::Windows::Forms::TextBox^  tb_ip;
	private: System::Windows::Forms::Button^  bt_listen;
	private: static System::Windows::Forms::RichTextBox^  rtb_main;
	private: System::ComponentModel::BackgroundWorker^  backgroundWorker1;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->tb_ip = (gcnew System::Windows::Forms::TextBox());
			this->bt_listen = (gcnew System::Windows::Forms::Button());
			this->rtb_main = (gcnew System::Windows::Forms::RichTextBox());
			this->backgroundWorker1 = (gcnew System::ComponentModel::BackgroundWorker());
			this->SuspendLayout();
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(13, 13);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(58, 13);
			this->label1->TabIndex = 0;
			this->label1->Text = L"IP Address";
			// 
			// tb_ip
			// 
			this->tb_ip->Location = System::Drawing::Point(92, 13);
			this->tb_ip->Name = L"tb_ip";
			this->tb_ip->Size = System::Drawing::Size(209, 20);
			this->tb_ip->TabIndex = 1;
			// 
			// bt_listen
			// 
			this->bt_listen->Location = System::Drawing::Point(308, 13);
			this->bt_listen->Name = L"bt_listen";
			this->bt_listen->Size = System::Drawing::Size(75, 23);
			this->bt_listen->TabIndex = 2;
			this->bt_listen->Text = L"Listen";
			this->bt_listen->UseVisualStyleBackColor = true;
			this->bt_listen->Click += gcnew System::EventHandler(this, &Server::bt_listen_Click);
			// 
			// rtb_main
			// 
			this->rtb_main->Location = System::Drawing::Point(16, 53);
			this->rtb_main->Name = L"rtb_main";
			this->rtb_main->Size = System::Drawing::Size(367, 192);
			this->rtb_main->TabIndex = 3;
			this->rtb_main->Text = L"";
			this->rtb_main->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Server::rtb_main_KeyPress);
			// 
			// backgroundWorker1
			// 
			this->backgroundWorker1->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &Server::backgroundWorker1_DoWork);
			// 
			// Server
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(408, 282);
			this->Controls->Add(this->rtb_main);
			this->Controls->Add(this->bt_listen);
			this->Controls->Add(this->tb_ip);
			this->Controls->Add(this->label1);
			this->Name = L"Server";
			this->Text = L"Server";
			this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &Server::Server_FormClosing);
			this->Load += gcnew System::EventHandler(this, &Server::Server_Load);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void rtb_main_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {
				 e->Handled = true;
	}
	private: static int numUser = 0;
	private: static int numClient = 0;
	private: static array<String^>^ lstUser = gcnew array<String^>(MAX);
	private: static array<String^>^ lstPass = gcnew array<String^>(MAX);
	private: static array<String^>^ lstUserClient = gcnew array<String^>(MAX);
	public: static array<Socket^>^ lstClient = gcnew array<Socket^>(MAX);
	public: static IPEndPoint^ ipe;
	public: static Thread^ listenClient;
	public: static Socket^ server;

			static void listen(Object^ obj)
			{
				Socket^ sk = (Socket^)obj;
				while (true)
				{
					try
					{
						array<unsigned char>^ buff = gcnew array<unsigned char>(1024);
						int recv = sk->Receive(buff);
						FormatterText::StructChat^ str = gcnew FormatterText::StructChat();
						str = str->Deserialize(buff);
						int type = str->type;

						if (type == CHAT)
						{
							add2RichTextBox(str->textChat, gcnew System::Drawing::Font("Arial", 10, FontStyle::Italic), Color::OliveDrab);
							for (int i = 0; i < numClient; ++i)
							{
								try{
									lstClient[i]->Send(buff, buff->Length, SocketFlags::None);
								}
								catch (Exception^ ex){}
							}
						}
						else if (type == PRIVATE_CHAT)
						{
							String^ user = "", ^ms = "";
							String^ text = str->textChat;
							int i = 0;
							while (i < text->Length && text[i] != ' ')
							{
								user += text[i];
								++i;
							}
							i++;
							ms = text->Substring(i);

							//Kiem tra user
							i = 0;
							while (i < numClient && lstUserClient[i] != user) i++;
							//Loi
							if (i >= numClient || lstClient[i] == nullptr)
							{
								str->type = -1;
								buff = str->Serialize();
								sk->Send(buff, buff->Length, SocketFlags::None);
							}
							else // gui ve client nhan
							{
								add2RichTextBox("Private chatting... " + str->textChat, gcnew System::Drawing::Font("Arial", 10, FontStyle::Italic), Color::OliveDrab);
								str->textChat = ms;
								buff = str->Serialize();
								sk->Send(buff, buff->Length, SocketFlags::None);
								lstClient[i]->Send(buff, buff->Length, SocketFlags::None);
							}
						}
						else if (type == SEND || type == FILE || type == END_FILE)
						{
							String^ user = "", ^ms = "";
							String^ text = str->textChat;
							int i = 0;
							while (i < text->Length && text[i] != ' ')
							{
								user += text[i];
								++i;
							}
							i++;
							ms = text->Substring(i);

							//Kiem tra user
							i = 0;
							while (i < numClient && lstUserClient[i] != user) i++;
							//Loi
							if (i >= numClient || lstClient[i] == nullptr)
							{
								str->type = -1;
								buff = str->Serialize();
								sk->Send(buff, buff->Length, SocketFlags::None);
							}
							else // gui ve client nhan
							{
								str->textChat = ms;
								buff = str->Serialize();
								lstClient[i]->Send(buff, buff->Length, SocketFlags::None);
							}
						}
						else
						{
							String^ user = "", ^pass = "";
							String^ text = str->textChat;
							int i = 0;
							while (i < text->Length && text[i] != ' ')
							{
								user += text[i];
								++i;
							}
							i++;
							pass = text->Substring(i);
							if (type == SIGN_IN)
							{
								i = 0;
								while (i < numUser && lstUser[i] != user) i++;
								if (i >= numUser || lstPass[i] != pass)
								{
									//Bao loi
									str->type = -1;
									buff = str->Serialize();
									sk->Send(buff, buff->Length, SocketFlags::None);
								}
								else
								{
									int j = numClient;
									while (j >= 0 && lstClient[j] != sk) --j;
									lstUserClient[j] = user;

									//tra ve thanh cong
									str->type = 0;
									buff = str->Serialize();
									sk->Send(buff, buff->Length, SocketFlags::None);

									//Thong bao cac client khac
									connect(sk, user);
								}
							}
							else if (type == SIGN_UP)
							{
								i = 0;
								while (i < numUser && lstUser[i] != user) i++;
								if (i >= numUser && i < MAX)
								{
									lstUser[i] = user;
									lstPass[i] = pass;
									++numUser;

									int j = numClient;
									while (j >= 0 && lstClient[j] != sk) --j;
									lstUserClient[j] = user;

									//bao thanh cong
									str->type = 0;
									buff = str->Serialize();
									sk->Send(buff, buff->Length, SocketFlags::None);

									//Thong bao cac client khac
									connect(sk, user);
								}
								else
								{
									//bao that bai
									str->type = -1;
									buff = str->Serialize();
									sk->Send(buff, buff->Length, SocketFlags::None);
								}
							}
							else if (type == DISCONNECT)
							{
								disconnect(sk);
							}
						}

					}
					catch (Exception^ ex)
					{
						disconnect(sk);
						return;
					}

				}
			}
			static	void disconnect(Socket^ sk)
			{
				int i = 0;
				while (i < numClient && lstClient[i] != sk) ++i;
				lstClient[i] = nullptr;
				String^ textTB;
				if (lstUserClient[i] != nullptr)
					textTB = lstUserClient[i] + " log off...\nDisconecting " + sk->RemoteEndPoint->ToString();
				else
					textTB = "Disconecting " + sk->RemoteEndPoint->ToString();
				rtb_main->SelectionFont = gcnew System::Drawing::Font("Arial", 10, FontStyle::Regular);
				rtb_main->SelectionColor = Color::DarkBlue;
				rtb_main->AppendText(textTB + "\n");

				array<unsigned char>^ buff = gcnew array<unsigned char>(1024);
				FormatterText::StructChat^ str = gcnew FormatterText::StructChat();
				str->type = CHAT;
				str->textChat = lstUserClient[i] + " log off...";
				str->font = gcnew System::Drawing::Font("Arial", 8, FontStyle::Italic);
				str->color = Color::Gray;
				buff = str->Serialize();
				for (int i = 0; i < numClient; ++i)
				{
					if (lstClient[i] != nullptr)
						lstClient[i]->Send(buff, buff->Length, SocketFlags::None);
				}
				lstUserClient[i] = nullptr;
			}
			static	void connect(Socket^ sk, String^ user)
			{
				rtb_main->SelectionFont = gcnew System::Drawing::Font("Arial", 10, FontStyle::Regular);
				rtb_main->SelectionColor = Color::BlueViolet;
				rtb_main->AppendText(user + " log in...\n");

				array<unsigned char>^ buff = gcnew array<unsigned char>(1024);
				FormatterText::StructChat^ str = gcnew FormatterText::StructChat();
				str->type = CHAT;
				str->textChat = user + " log in...";
				str->font = gcnew System::Drawing::Font("Arial", 8, FontStyle::Italic);
				str->color = Color::Gray;
				buff = str->Serialize();
				for (int i = 0; i < numClient; ++i)
				{
					if (lstClient[i] != nullptr && lstClient[i] != sk)
						lstClient[i]->Send(buff, buff->Length, SocketFlags::None);
				}
			}
			static void add2RichTextBox(String^ text, System::Drawing::Font^ font, System::Drawing::Color^ color)
			{
				rtb_main->SelectionFont = font;
				rtb_main->SelectionColor = (Color)color;
				rtb_main->AppendText(text + "\n");
			}
	private: System::Void bt_listen_Click(System::Object^  sender, System::EventArgs^  e) {
				 ipe = gcnew IPEndPoint(IPAddress::Parse(tb_ip->Text), 0511);
				 backgroundWorker1->WorkerSupportsCancellation = true;
				 backgroundWorker1->RunWorkerAsync();
				 bt_listen->Enabled = false;
	}
	private: System::Void Server_Load(System::Object^  sender, System::EventArgs^  e) {
				 server = gcnew Socket(AddressFamily::InterNetwork, SocketType::Stream, ProtocolType::Tcp);

	}
	private: System::Void backgroundWorker1_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e) {
				 server->Bind(ipe);
				 server->Listen(10);
				 String^ text = "Server is listening...";
				 System::Drawing::Font^ font = gcnew System::Drawing::Font("Arial", 12, FontStyle::Bold);
				 System::Drawing::Color^ color = Color::DeepPink;
				 add2RichTextBox(text, font, color);
				 while (true)
				 {
					 Socket^ clientAccept = server->Accept();

					 addLstClient(clientAccept);

					 listenClient = gcnew Thread(gcnew ParameterizedThreadStart(Sever::Server::listen));
					 listenClient->IsBackground = true;
					 listenClient->Start(clientAccept);

					 String^ textTB = "Accept conecting " + clientAccept->RemoteEndPoint->ToString();
					 System::Drawing::Font^ fontTB = gcnew System::Drawing::Font("Arial", 10, FontStyle::Regular);
					 System::Drawing::Color^ colorTB = Color::BlueViolet;
					 add2RichTextBox(textTB, fontTB, colorTB);
				 }
	}
			 void addLstClient(Socket^ sk)
			 {
				 int i = 0;
				 while (i < numClient && lstClient[i] != nullptr) ++i;
				 if (i >= numClient && i < MAX)
				 {
					 lstClient[numClient++] = sk;
				 }
				 else
					 lstClient[i] = sk;
			 }
	private: System::Void Server_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e) {
				 if (backgroundWorker1->IsBusy)
				 {
					 backgroundWorker1->CancelAsync();
				 }
	}
	};
}
